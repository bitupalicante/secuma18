## DESCRIPCIÓN DE LOS RETOS:

**Nombre Reto** | **Nivel** | **Puntos** | **Tecnología**
------------ | ------------- | ------------- | ------------- |
RonsCoffee | Bajo | 150 | PHP + MySQL
FBI | Medio | 300 | PHP
DeathNode | Alto | 500 | NodeJS

<br>

## Agradecimientos:

Quiero agradecer a [**Rolo**](https://twitter.com/rolomijan?lang=es) que me inspiró con el CTF de Interferencias para el reto de FBI.<br>
De igual forma, quiero agradecer al grupo de [**Ka0labs**](https://twitter.com/ka0labs_?lang=es) que me inspiraron a adentrarme en el mundo de Nodejs con la prueba de Tindermon del CTF de Navaja Negra.