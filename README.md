# CTF - Secuma18

## Un pequeño repaso.
[**Secuma**](https://secuma.weebly.com/) es un Evento de Ciberseguridad que se hizo en la Universidad de Málaga el 15 de Noviembre de 2018.<br><br>
El equipo de Bitup hizo un CTF para el Evento, combinando alguna de las ramas existentes de la Seguridad Informática, que hoy os traemos a dicho repositorio.<br><br>

Tanto los Writeups o soluciones a los retos, así como algunos de los **códigos fuentes** para montar el reto y practicar en un entorno local.

## Distribuciones, alteraciones y/o modificaciones.

Desde el equipo de Bitup os traemos este contenido con el fin de que todos podáis utilizarlo, modificarlo y/o distribuirlo.
No hay más restricción que la de compartirlo e indicar el autor inicial del mismo.

