## DESCRIPCIÓN DE LOS RETOS:

**Nombre Reto** | **Nivel** | **Puntos**
------------ | ------------- | ------------- 
Cl4s1c P4r4n01d | Bajo | 150
Fucks0ci3ty | Medio | 300

<br>

### 1. Writeup Cl4s1c P4r4n01d.

> "Tenemos que ayudar a la agente DiPierro, ha conseguido interceptar 3 mensajes
de algunos usuarios, pero cada uno usa un cifrado distinto. ¿Puedes ayudarla? 
Se encontró un texto sin cifrar que decía: La union es la clave."<br>

Integridad Writeup PDF.

> SHA256: 	179392de46267a6bf35b612d1a49d35081658255dba6923070a56706a16bce49<br>
  Nombre: 	Cl4s1c-P4r4n01d.pdf<br>
  Detecciones: 	0 / 58<br>
  Fecha de análisis: 	2018-11-27 15:16:30 UTC <br>

Check Virus Total [**Report**](https://www.virustotal.com/#/file/179392de46267a6bf35b612d1a49d35081658255dba6923070a56706a16bce49/detection)


### 2. Writeup Fuck Soci3ty.

> "I think she has to be the key to all this..."<br>

Integridad Writeup PDF.

> SHA256: 	edd054fc9afbcb15af3c150c1e037f94c6b8e09db854d34ecd3eb1fe76e16072<br>
  Nombre: 	Writeup-fuck-soci3ty.pdf<br>
  Detecciones: 	0 / 58<br>
  Fecha de análisis: 	2018-11-27 16:43:33 UTC <br>

Check Virus Total [**Report**](https://www.virustotal.com/#/file/edd054fc9afbcb15af3c150c1e037f94c6b8e09db854d34ecd3eb1fe76e16072/detection)


