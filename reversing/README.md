# Reversing

## Agradecimientos:

Quiero agradecer al compañero [**Mrp314**](https://t.me/@Mrp314) por aportar los retos de reversing 1 y 3, así como por toda la ayuda aportada!

## Writeup (retos 1,2,3)

Sinceramente, después de ver el Writeup del compañero **Tony Palma**, me parece una ofensa
a la Ingenieria Inversa no hacer referencia a las soluciones que él nos aportó.
Podeis encontrar el Writeup completo de los 3 retos de reversing en su blog [**xbytemx**](https://xbytemx.github.io/post/secuma2018-writeups/), muchas gracias por aportarnos estas
soluciones tan claras e interesantes! Eres un máquina!


### CS30
Puntos: `150`
Flag: `secuma18{OnE_oR_zEro}`
Adjunto: [fsociety](CS30/fsociety) (MD5: 6e1eb141ab908fb2646ed9998f1ec900)

## breakCorp
Puntos: `300`
Flag: `secuma18{eVeRyThInghaPPendSBYaReaSoN}`
Adjunto: [breakCorp](breakCorp/breakCorp) (MD5: c3ca32763ec9abaff92d181fa686f9e3)

### Stranger Files
Puntos: `500`
Flag: `secuma18{WeLlCoMeToThEr3AlW0rLD}`
Adjunto: [SimulacroySimulacion](Stranger_Files/SimulacroySimulacion) (MD5: db1e1ce8050ec6f7e1d67fb88a00e63c)