## QUIZZ:

**Nombre Reto** | **Descripción** | **Puntos** | **Solución**
------------ | ------------- | ------------- | ------------- |
Elliot Alderson | ¿Cuántos miligramos de morfina consume Elliot al día? | 10 | secuma18{30}
Krista Gordon | ¿Cuál es la contraseña de la psicóloga de Elliot cuando este la hackea? | 10 | secuma18{dylan_2791}
FSociety | ¿Cuál es el nombre del fichero (incluida la extensión) que Elliot encuentra tras el hackeo a E Corp? | 10 | secuma18{fsociety00.dat}
Time | ¿Cuál es la película favorita de Elliot Alderson? | 10 | secuma18{Back_To_The_Future_II}
Allsafe | ¿Cuál el número de empleado de Elliot Alderson en Allsafe? | 10 | secuma18{ER28-0652}
Friends | ¿Cuál es el nombre de la amiga de la infancia de Elliot? | 10 | secuma18{Angela_Moss}
<br>
<br>

 *Algunas soluciones también admitian otras combinaciones entre mayúsculas y minúsculas para mayor flexibilidad.<br>
 Challenge by [**@fm_trujillo**](https://twitter.com/fm_trujillo),  write up by [**@Verdej0**](https://twitter.com/verdej0)
