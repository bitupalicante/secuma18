## DESCRIPCIÓN DE LOS RETOS:

**Nombre Reto** | **Nivel** | **Puntos**
------------ | ------------- | ------------- 
Discographyde | Bajo | 150
What is poetry? | Medio | 300
0ld_m3m0r13s | Alto | 500

<br>

## Agradecimientos:

Quiero agradecer al compañero [**MrCokito**](https://twitter.com/mrcokito) por aportar el reto **"What is poetry?"** (reto 2) de Stego y toda la ayuda aportada!
No olvideis visitar su blog de ciberseguridad [**HackingLethani**](http://hackinglethani.com/)<br>

Y también quiero dar un pequeño agradecimiento desde aquí a [**KALRONG**](https://twitter.com/KALRONG) por reportarnos el fallo con el reto 3 **"0ld_m3m0r13s"** donde teníamos un fallo con la imagen.


### 1. Writeup Discographyde.

Descripción del Reto:

> When I hack...<br>
  There is always a tone for each key, there is always a melody that reminds you of that moment of tension, adrenaline ... everything.<br>
  And that makes us unable to avoid hiding our darkest secrets in the deepest of those songs.

Fichero adjunto: [comprimido](discographyde/macquayle.7z) (MD5: 1d73e2e76ec86229e9245d0c6fe5f833)

Integridad Writeup PDF.

> SHA256: 	6087759f9984a1f8d8d8b70b9a9ee53ad1de1e0b1369aab781b8105d2386c565<br>
  Nombre: 	writeup-discographyde.pdf<br>
  Detecciones: 	0 / 58<br>
  Fecha de análisis: 	2018-11-27 01:45:26 UTC <br>

Check Virus Total [**Report**](https://www.virustotal.com/#/file/6087759f9984a1f8d8d8b70b9a9ee53ad1de1e0b1369aab781b8105d2386c565/detection)


### 2. Writeup What is poetry?

Descripción del Reto:

> Dices, mientras clavas tu pupila azul en mi pupila.<br>
  Nota: El fichero está comprimido con 7-zip.<br>
  Formato flag: secuma18{__flag__} 

Fichero adjunto: [comprimido](what_is_poetry/What_is_poetry.7z) (MD5: 658e0fd0f28bd1ad62fd1b41599f8667)

Integrity Writeup PDF.

> SHA-256	eade469d550b79c4d86de6d078a72a3c24983eecbfd8d09128ec4f7626d3586c<br>
  File name	writeup-what-is-poetry.pdf<br>
  Detecciones: 	0 / 58<br>
  Last analysis	2018-11-27 02:28:55 UTC<br>

Check Virus Total [**Report**](https://www.virustotal.com/#/file/eade469d550b79c4d86de6d078a72a3c24983eecbfd8d09128ec4f7626d3586c/detection)


### 3. Writeup 0ld_m3m0r13s.

Descripción del Reto:

> My father and I were very close. He was an example to follow for me.<br>
  But ... one day I failed him. And from that day he stopped talking to me and looking at me, even the night he died. He did not say anything to me.<br>
  I remember ... he always told me that people try hard to hide their fears and shadows, but with a little light, everything is discovered.


Fichero adjunto: [Imagen](old_memories/r3m3mb3rme.jpg)  (MD5: 245a5e8be2ca1674f80a619339e44055)

Integrity Writeup PDF.

> SHA-256	b5d3900fa4fb65f97c420b5a2a1681755218c1b44283acccd74064749e47e577<br>
  File name	writeup-0ld-m3m0r13s.pdf<br>
  Detecciones: 	0 / 58<br>
  Last analysis	2018-11-27 01:46:16 UTC<br>

Check Virus Total [**Report**](https://www.virustotal.com/#/file/b5d3900fa4fb65f97c420b5a2a1681755218c1b44283acccd74064749e47e577/detection)
